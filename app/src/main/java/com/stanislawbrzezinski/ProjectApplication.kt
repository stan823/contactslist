package com.stanislawbrzezinski

import android.app.Application
import android.content.Context


/**
 * Created by Stanisław Brzeziński on 30/11/2018.
 * Copyrights Stanisław Brzeziński
 */
class ProjectApplication:Application(){

    companion object {
        lateinit var context:Context
    }
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        context = baseContext
    }
}