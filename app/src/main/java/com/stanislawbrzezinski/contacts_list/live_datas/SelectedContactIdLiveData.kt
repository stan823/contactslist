package com.stanislawbrzezinski.contacts_list.live_datas

import android.arch.lifecycle.MutableLiveData


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class SelectedContactIdLiveData:MutableLiveData<String>()