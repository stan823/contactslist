package com.stanislawbrzezinski.contacts_list.live_datas

import android.arch.lifecycle.MutableLiveData
import com.stanislawbrzezinski.contacts_list.statics.Routes


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class NavigationLiveData:MutableLiveData<Routes>(){

    fun resetNavigation(){
        postValue(null)
    }
}