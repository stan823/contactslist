package com.stanislawbrzezinski.contacts_list.live_datas

import android.arch.lifecycle.MutableLiveData
import com.stanislawbrzezinski.contacts_list.models.ContactModel


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class ContactsLiveData:MutableLiveData<List<ContactModel>>(){
    init {
        value = mutableListOf()
    }
}