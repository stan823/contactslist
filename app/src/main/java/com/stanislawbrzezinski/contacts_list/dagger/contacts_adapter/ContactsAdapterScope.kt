package com.stanislawbrzezinski.contacts_list.dagger.contacts_adapter

import javax.inject.Scope


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
@Scope
annotation class ContactsAdapterScope