package com.stanislawbrzezinski.contacts_list.dagger.app

import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.SelectedContactIdLiveData
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppLiveDataModule {

    @Provides
    @Singleton
    fun provideContactsLiveData() = ContactsLiveData()

    @Provides
    @Singleton
    fun provideNavigationLiveData() = NavigationLiveData()

    @Provides
    @Singleton
    fun provideSelectedIdLiveData() = SelectedContactIdLiveData()
}