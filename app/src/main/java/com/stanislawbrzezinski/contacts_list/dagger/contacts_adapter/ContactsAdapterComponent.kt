package com.stanislawbrzezinski.contacts_list.dagger.contacts_adapter

import com.stanislawbrzezinski.contacts_list.dagger.app.AppComponent
import com.stanislawbrzezinski.contacts_list.dagger.app.appComponent
import com.stanislawbrzezinski.contacts_list.ui.contacts_adapter.ContactsAdapter
import dagger.Component


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
@ContactsAdapterScope
@Component(modules = [ContactsAdapterModule::class],
        dependencies = [AppComponent::class])
interface ContactsAdapterComponent {
    fun inject(adapter:ContactsAdapter)
}

val contactsAdapterComponent:ContactsAdapterComponent by lazy{
    DaggerContactsAdapterComponent
            .builder()
            .appComponent(appComponent)
            .build()
}