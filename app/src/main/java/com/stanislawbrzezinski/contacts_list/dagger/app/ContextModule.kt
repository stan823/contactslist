package com.stanislawbrzezinski.contacts_list.dagger.app

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/**
 * Created by Stanisław Brzeziński on 30/11/2018.
 * Copyrights Stanisław Brzeziński
 */
@Module
class ContextModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext()
        = context
}