package com.stanislawbrzezinski.contacts_list.dagger.main_activity

import com.stanislawbrzezinski.contacts_list.ui.MainActivity
import com.stanislawbrzezinski.contacts_list.dagger.app.AppComponent
import com.stanislawbrzezinski.contacts_list.dagger.app.appComponent
import dagger.Component


/**
 * Created by Stanisław Brzeziński on 30/11/2018.
 * Copyrights Stanisław Brzeziński
 */
@Component(modules = [MainActivityModule::class],
        dependencies = [AppComponent::class])
@MainActivityScope
interface MainActivityComponent {
    fun inject(mainActivity: MainActivity)
}

val contactsComponent:MainActivityComponent by lazy {
    DaggerMainActivityComponent
            .builder()
            .appComponent(appComponent)
            .build()
}