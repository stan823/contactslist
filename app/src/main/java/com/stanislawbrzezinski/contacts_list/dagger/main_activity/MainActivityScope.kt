package com.stanislawbrzezinski.contacts_list.dagger.main_activity

import javax.inject.Scope


/**
 * Created by Stanisław Brzeziński on 30/11/2018.
 * Copyrights Stanisław Brzeziński
 */
@Scope
annotation class MainActivityScope