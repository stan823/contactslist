package com.stanislawbrzezinski.contacts_list.dagger.app

import android.content.Context
import com.stanislawbrzezinski.ProjectApplication
import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.SelectedContactIdLiveData
import dagger.Component
import javax.inject.Singleton


/**
 * Created by Stanisław Brzeziński on 20/11/2018.
 * Copyrights Stanisław Brzeziński
 */
@Component(modules = [ContextModule::class, AppLiveDataModule::class])
@Singleton
interface AppComponent {
    fun context(): Context
    fun contactsLiveData(): ContactsLiveData
    fun navigationLiveData(): NavigationLiveData
    fun selectedIdLiveData(): SelectedContactIdLiveData
}

val appComponent: AppComponent by lazy {
    DaggerAppComponent
            .builder()
            .contextModule(
                    ContextModule(ProjectApplication.context)
            )
            .build()
}