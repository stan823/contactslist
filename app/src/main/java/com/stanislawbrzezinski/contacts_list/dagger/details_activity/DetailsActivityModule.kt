package com.stanislawbrzezinski.contacts_list.dagger.details_activity

import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.SelectedContactIdLiveData
import com.stanislawbrzezinski.contacts_list.view_model.DetailsActivityViewModel
import com.stanislawbrzezinski.contacts_list.view_model.DetailsActivityViewModelImpl
import dagger.Module
import dagger.Provides


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
@Module
class DetailsActivityModule {

    @Provides
    @DetailsActivityScope
    fun provideViewModel(
            navigationLiveData: NavigationLiveData,
            selectedContactIdLiveData: SelectedContactIdLiveData,
            contactsLiveData: ContactsLiveData
    ): DetailsActivityViewModel {
        return DetailsActivityViewModelImpl(
                navigationLiveData,
                selectedContactIdLiveData,
                contactsLiveData)
    }
}