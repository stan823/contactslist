package com.stanislawbrzezinski.contacts_list.dagger.main_activity

import android.content.Context
import com.stanislawbrzezinski.contacts_list.data_sources.ContactsProvider
import com.stanislawbrzezinski.contacts_list.data_sources.ContactsProviderImpl
import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.view_model.MainActivityViewModel
import com.stanislawbrzezinski.contacts_list.view_model.MainActivityViewModelImpl
import dagger.Module
import dagger.Provides


/**
 * Created by Stanisław Brzeziński on 30/11/2018.
 * Copyrights Stanisław Brzeziński
 */
@Module
class MainActivityModule {

    @MainActivityScope
    @Provides
    fun provideContactsProvider(context: Context): ContactsProvider {
        return ContactsProviderImpl(context)
    }

    @MainActivityScope
    @Provides
    fun provideViewModel(
            provider: ContactsProvider,
            navigationLiveData: NavigationLiveData,
            contactsLiveData: ContactsLiveData): MainActivityViewModel {
        return MainActivityViewModelImpl(
                provider,
                navigationLiveData,
                contactsLiveData)
    }
}