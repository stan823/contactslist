package com.stanislawbrzezinski.contacts_list.dagger.contacts_adapter

import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.SelectedContactIdLiveData
import com.stanislawbrzezinski.contacts_list.view_model.ContactsAdapterViewModel
import com.stanislawbrzezinski.contacts_list.view_model.ContactsAdapterViewModelImpl
import dagger.Module
import dagger.Provides


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
@Module
class ContactsAdapterModule {

    @ContactsAdapterScope
    @Provides
    fun provideViewModel(
            contactsLiveData: ContactsLiveData,
            navigationLiveData: NavigationLiveData,
            selectedContactIdLiveData: SelectedContactIdLiveData
    ):ContactsAdapterViewModel{
        return ContactsAdapterViewModelImpl(
                contactsLiveData,
                navigationLiveData,
                selectedContactIdLiveData)
    }
}