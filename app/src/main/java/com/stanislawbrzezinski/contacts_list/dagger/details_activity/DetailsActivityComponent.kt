package com.stanislawbrzezinski.contacts_list.dagger.details_activity

import com.stanislawbrzezinski.contacts_list.dagger.app.AppComponent
import com.stanislawbrzezinski.contacts_list.dagger.app.appComponent
import com.stanislawbrzezinski.contacts_list.ui.DetailsActivity
import dagger.Component


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
@Component(modules = [DetailsActivityModule::class],
        dependencies = [AppComponent::class])
@DetailsActivityScope
interface DetailsActivityComponent {
    fun inject(activity:DetailsActivity)
}

val detailsActivityComponent:DetailsActivityComponent by lazy{
    DaggerDetailsActivityComponent
            .builder()
            .appComponent(appComponent)
            .build()
}