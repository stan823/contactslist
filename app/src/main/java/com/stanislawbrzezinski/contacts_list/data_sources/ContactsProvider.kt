package com.stanislawbrzezinski.contacts_list.data_sources

import com.stanislawbrzezinski.contacts_list.models.ContactModel


/**
 * Created by Stanisław Brzeziński on 30/11/2018.
 * Copyrights Stanisław Brzeziński
 */
interface ContactsProvider {
    suspend fun fetchContacts():List<ContactModel>
}