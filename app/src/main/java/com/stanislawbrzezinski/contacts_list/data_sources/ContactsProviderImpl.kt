package com.stanislawbrzezinski.contacts_list.data_sources

import android.provider.ContactsContract
import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import com.stanislawbrzezinski.contacts_list.DebugFlags
import com.stanislawbrzezinski.contacts_list.models.ContactModel


/**
 * Created by Stanisław Brzeziński on 30/11/2018.
 * Copyrights Stanisław Brzeziński
 */
class ContactsProviderImpl(private val context: Context)
    : ContactsProvider {

    override suspend fun fetchContacts(): List<ContactModel> {
        if (DebugFlags.isTest) {
            return createTestResults()
        }
        val contacts = mutableListOf<ContactModel>()

        val contactsResolver = context.getContentResolver()
        val cur = contactsResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
        cur?.let { cursor ->

            if (cursor.count > 0) {
                while (cursor.moveToNext()) {
                    val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                    val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                    val imageThumb = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI))
                    val image = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI))
                    var phones: List<String?>? = getPhones(cursor, contactsResolver, id)
                    var emails: List<String?>? = getEmails(contactsResolver, id)

                    if (id != null && name != null) {
                        val model = ContactModel(
                                id = id,
                                name = name,
                                imageThumbUrl = imageThumb,
                                image = image,
                                phones = phones,
                                emails = emails
                        )

                        contacts.add(model)
                    }
                }
            }
        }
        cur?.close()
        return contacts
    }

    private fun getEmails(cr: ContentResolver, id: String?): List<String?>? {
        var emails: List<String?>? = null
        val emailsCursor = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                "${ContactsContract.CommonDataKinds.Email.CONTACT_ID} = $id", null, null)
        if (emailsCursor != null && emailsCursor.count > 0) {
            emails = mutableListOf()
            while (emailsCursor.moveToNext()) {
                // This would allow you get several email addresses
                val emailAddress = emailsCursor.getString(emailsCursor
                        .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                emails.add(emailAddress)

            }

        }
        emailsCursor?.close()
        return emails
    }

    private fun getPhones(cur: Cursor, cr: ContentResolver, id: String): List<String?>? {
        var phones: List<String?>? = null
        if (cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
            phones = mutableListOf()
            val pCur = cr.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                    arrayOf<String>(id), null)
            pCur?.let { pCur ->

                while (pCur.moveToNext()) {
                    val phoneNo = pCur.getString(pCur.getColumnIndex(
                            ContactsContract.CommonDataKinds.Phone.NUMBER))
                    phones.add(phoneNo)
                }
            }
            pCur.close()
        }
        return phones
    }

    private fun createTestResults(): List<ContactModel>{
        return listOf(
                ContactModel(id="1",
                        name="name1",
                        phones = listOf("phone1","phone2"),
                        emails = listOf("email1")
                ),
                ContactModel(id="2",
                        name="name2"
                )
        )
    }
}