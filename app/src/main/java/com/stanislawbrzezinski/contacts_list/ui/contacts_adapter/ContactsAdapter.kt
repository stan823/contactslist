package com.stanislawbrzezinski.contacts_list.ui.contacts_adapter

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.Observer
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.stanislawbrzezinski.contacts_list.R
import com.stanislawbrzezinski.contacts_list.dagger.contacts_adapter.contactsAdapterComponent
import com.stanislawbrzezinski.contacts_list.models.ContactsEntryModel
import com.stanislawbrzezinski.contacts_list.view_model.ContactsAdapterViewModel
import javax.inject.Inject


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class ContactsAdapter
    : RecyclerView.Adapter<ContactsViewHolder>(),
        LifecycleObserver {

    @Inject
    lateinit var viewModel: ContactsAdapterViewModel

    private var items: List<ContactsEntryModel> = mutableListOf()

    val itemsObserver = Observer<List<ContactsEntryModel>?> {
        items = it ?: mutableListOf()
        notifyDataSetChanged()
    }

    init {
        contactsAdapterComponent.inject(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_contact, null)
        return ContactsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        if (position < items.size) {
            items[position].let { item ->
                holder.label = item.label
                holder.profileImageUrl = item.imageUrl
                holder.clickCallback {
                    viewModel.selectId(item.id)
                }
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart(){
        items = viewModel.getContacts() ?: mutableListOf()
        viewModel.observeContacts(itemsObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop(){
        viewModel.removeContactsObserver(itemsObserver)
    }


}