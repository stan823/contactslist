package com.stanislawbrzezinski.contacts_list.ui.details_adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class NonScrollableLayoutManager(context: Context)
    : LinearLayoutManager(context) {

    override fun canScrollVertically(): Boolean {
        return false
    }
}