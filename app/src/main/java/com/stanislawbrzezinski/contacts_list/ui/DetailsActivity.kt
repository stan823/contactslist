package com.stanislawbrzezinski.contacts_list.ui

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.squareup.picasso.Picasso
import com.stanislawbrzezinski.contacts_list.R
import com.stanislawbrzezinski.contacts_list.dagger.details_activity.detailsActivityComponent
import com.stanislawbrzezinski.contacts_list.models.ContactModel
import com.stanislawbrzezinski.contacts_list.ui.details_adapter.DetailsAdapter
import com.stanislawbrzezinski.contacts_list.ui.details_adapter.NonScrollableLayoutManager
import com.stanislawbrzezinski.contacts_list.view_model.DetailsActivityViewModel
import kotlinx.android.synthetic.main.activity_details.*
import javax.inject.Inject

class DetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var detailsActivityViewModel: DetailsActivityViewModel

    private val detailsAdapter = DetailsAdapter()

    private val contactObserver = Observer<ContactModel?> {
        it?.let { contact ->
            tvName.text = contact.name
            contact.image?.let { image ->
                Picasso.get().load(image).into(ivProfile)
            }
        }
    }

    private val detailsObserver = Observer<List<String>> {
        detailsAdapter.setItems(it ?: ArrayList())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        detailsActivityComponent.inject(this)
        detailsActivityViewModel.resetNavigation()
        detailsActivityViewModel.observeContact(this, contactObserver)
        detailsActivityViewModel.observeDetailsList(this,detailsObserver)

        setUpAdapter()

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)

        }
    }

    private fun setUpAdapter() {
        detailsList.layoutManager = NonScrollableLayoutManager(this)
        detailsList.adapter = detailsAdapter
    }
}
