package com.stanislawbrzezinski.contacts_list.ui.contacts_adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.squareup.picasso.Picasso
import com.stanislawbrzezinski.contacts_list.R
import com.stanislawbrzezinski.contacts_list.utils.CircleTransform
import kotlinx.android.synthetic.main.cell_contact.view.*


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class ContactsViewHolder(itemView: View)
    : RecyclerView.ViewHolder(itemView) {

    private val tvName = itemView.tvName
    private val ivProfile = itemView.ivProfile
    private val _profileImageUrl: String? = null
    private val accountDrawable = itemView
            .context
            .resources
            .getDrawable(R.drawable.ic_account_circle_grey_24dp)

    var profileImageUrl: String?
        get() = _profileImageUrl
        set(value) {
            if (value == null) {
                ivProfile.setImageDrawable(accountDrawable)
            } else {
                Picasso.get()
                        .load(value)
                        .transform(CircleTransform())
                        .into(ivProfile)
            }
        }

    var label: String?
        get() = tvName.text.toString()
        set(value) {
            tvName.text = value ?: ""
        }

    fun clickCallback(callback:()->Unit){
        itemView.card.setOnClickListener {
            callback()
        }
    }
}