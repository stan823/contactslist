package com.stanislawbrzezinski.contacts_list.ui.details_adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.stanislawbrzezinski.contacts_list.R


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class DetailsAdapter:RecyclerView.Adapter<DetailsViewHolder>(){

    private var items:List<String> = mutableListOf()

    fun setItems(items:List<String>){
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.cell_detail_entry,null)
        return DetailsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) {
        if (items.size>position){
            holder.label.text = items[position]
        }
    }
}