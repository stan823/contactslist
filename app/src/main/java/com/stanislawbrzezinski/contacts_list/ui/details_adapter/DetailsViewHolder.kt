package com.stanislawbrzezinski.contacts_list.ui.details_adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.cell_detail_entry.view.*


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class DetailsViewHolder(itemView: View)
    :RecyclerView.ViewHolder(itemView) {

    val label = itemView.tvEntry
}