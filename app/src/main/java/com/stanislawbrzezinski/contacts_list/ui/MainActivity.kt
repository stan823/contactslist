package com.stanislawbrzezinski.contacts_list.ui

import android.Manifest
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.LinearLayoutManager
import com.stanislawbrzezinski.contacts_list.R
import com.stanislawbrzezinski.contacts_list.dagger.main_activity.contactsComponent
import com.stanislawbrzezinski.contacts_list.statics.Routes
import com.stanislawbrzezinski.contacts_list.ui.contacts_adapter.ContactsAdapter
import com.stanislawbrzezinski.contacts_list.view_model.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private val PERMISSIONS_REQUEST_CODE = 5643
    @Inject
    lateinit var viewModel: MainActivityViewModel

    private val routesObserver = Observer<Routes> {
        when (it) {
            Routes.MAIN_TO_DETAILS -> openDetailsActivity()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        contactsComponent.inject(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions()
        } else {

            viewModel.fetchContacts()
        }
        viewModel.observeNavigationLiveData(this, routesObserver)

        setUpAdapter()
    }

    private fun checkPermissions() {
        if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission_group.CONTACTS) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS),
                    PERMISSIONS_REQUEST_CODE)
        } else {
            viewModel.fetchContacts()
        }
    }

    private fun setUpAdapter() {
        val adapter = ContactsAdapter()
        entriesList.adapter = adapter
        entriesList.layoutManager = LinearLayoutManager(this)
        lifecycle.addObserver(adapter)
    }

    private fun openDetailsActivity() {
        startActivity(Intent(this, DetailsActivity::class.java))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST_CODE) {

            val writePermission = ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_CONTACTS
            )

            val readPermission = ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_CONTACTS
            )



            if (readPermission == PackageManager.PERMISSION_GRANTED &&
                    writePermission == PackageManager.PERMISSION_GRANTED) {
                viewModel.fetchContacts()
            }
        }
    }
}
