package com.stanislawbrzezinski.contacts_list.models


/**
 * Created by Stanisław Brzeziński on 30/11/2018.
 * Copyrights Stanisław Brzeziński
 */
data class ContactModel(
        val id: String,
        val name: String,
        val phones: List<String?>? = null,
        val emails: List<String?>? = null,
        val imageThumbUrl: String? = null,
        val image: String? = null
)

fun ContactModel.toContactsEntry(): ContactsEntryModel {
    return ContactsEntryModel(
            this.id,
            this.name,
            this.imageThumbUrl
    )
}