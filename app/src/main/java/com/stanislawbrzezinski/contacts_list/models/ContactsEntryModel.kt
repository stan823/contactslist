package com.stanislawbrzezinski.contacts_list.models


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
data class ContactsEntryModel(
        val id:String,
        val label: String,
        val imageUrl: String?
)
