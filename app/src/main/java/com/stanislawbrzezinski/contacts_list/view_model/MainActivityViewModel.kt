package com.stanislawbrzezinski.contacts_list.view_model

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import com.stanislawbrzezinski.contacts_list.statics.Routes


/**
 * Created by Stanisław Brzeziński on 20/11/2018.
 * Copyrights Stanisław Brzeziński
 */
interface MainActivityViewModel {

    fun fetchContacts()
    fun observeNavigationLiveData(owner: LifecycleOwner,observer: Observer<Routes>)
}