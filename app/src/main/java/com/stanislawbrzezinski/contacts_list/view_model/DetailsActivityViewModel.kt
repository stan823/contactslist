package com.stanislawbrzezinski.contacts_list.view_model

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import com.stanislawbrzezinski.contacts_list.models.ContactModel


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
interface DetailsActivityViewModel{
    fun observeContact(owner: LifecycleOwner,observer: Observer<ContactModel?>)
    fun observeDetailsList(owner: LifecycleOwner,observer: Observer<List<String>>)
    fun resetNavigation()
}