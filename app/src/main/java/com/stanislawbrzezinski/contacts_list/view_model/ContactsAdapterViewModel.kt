package com.stanislawbrzezinski.contacts_list.view_model

import android.arch.lifecycle.Observer
import com.stanislawbrzezinski.contacts_list.models.ContactsEntryModel


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
interface ContactsAdapterViewModel {
    fun observeContacts(observer: Observer<List<ContactsEntryModel>?>)
    fun removeContactsObserver(observer: Observer<List<ContactsEntryModel>?>)
    fun getContacts():List<ContactsEntryModel>?
    fun selectId(id:String)
}