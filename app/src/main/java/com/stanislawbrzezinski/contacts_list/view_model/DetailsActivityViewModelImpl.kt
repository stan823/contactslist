package com.stanislawbrzezinski.contacts_list.view_model

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.arch.lifecycle.Transformations
import android.support.annotation.VisibleForTesting
import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.SelectedContactIdLiveData
import com.stanislawbrzezinski.contacts_list.models.ContactModel


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class DetailsActivityViewModelImpl(
        private val navigationLiveData: NavigationLiveData,
        selectedContactIdLiveData: SelectedContactIdLiveData,
        contactsLiveData: ContactsLiveData)
    : DetailsActivityViewModel {

    @VisibleForTesting
    val detailsMap: (ContactModel?) -> List<String> = {

        val result = mutableListOf<List<String>>()
        result.add(it?.phones
                ?.map { it ?: "" }
                ?.filterNot { it == "" }
                ?: ArrayList())

        result.add(it?.emails
                ?.map { it ?: "" }
                ?.filterNot { it == "" }
                ?: ArrayList())

        result.flatten()

    }

    private
    val uiLiveData = Transformations.map(selectedContactIdLiveData) {
         contactsLiveData.value?.find { it.id == selectedContactIdLiveData.value ?: "1" }
    }

    private val detailsLiveData = Transformations.map(uiLiveData, detailsMap)

    override fun observeContact(owner: LifecycleOwner, observer: Observer<ContactModel?>) {
        uiLiveData.observe(owner, observer)
    }

    override fun observeDetailsList(owner: LifecycleOwner, observer: Observer<List<String>>) {
        detailsLiveData.observe(owner, observer)
    }

    override fun resetNavigation() {
        navigationLiveData.resetNavigation()
    }
}