package com.stanislawbrzezinski.contacts_list.view_model

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import com.stanislawbrzezinski.contacts_list.data_sources.ContactsProvider
import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.statics.Routes
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


/**
 * Created by Stanisław Brzeziński on 20/11/2018.
 * Copyrights Stanisław Brzeziński
 */
class MainActivityViewModelImpl(
        private val provider: ContactsProvider,
        private val navigationLiveData: NavigationLiveData,
        private val contactsLiveData: ContactsLiveData)
    : MainActivityViewModel {


    override fun fetchContacts() {
        GlobalScope.launch {
            val contacts = provider.fetchContacts()
            contactsLiveData.postValue(contacts)
        }
    }

    override fun observeNavigationLiveData(owner: LifecycleOwner, observer: Observer<Routes>) {
        navigationLiveData.observe(owner, observer)
    }

}