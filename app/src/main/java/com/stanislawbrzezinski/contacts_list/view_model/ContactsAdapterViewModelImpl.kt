package com.stanislawbrzezinski.contacts_list.view_model

import android.arch.lifecycle.Observer
import android.arch.lifecycle.Transformations
import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.SelectedContactIdLiveData
import com.stanislawbrzezinski.contacts_list.models.ContactsEntryModel
import com.stanislawbrzezinski.contacts_list.models.toContactsEntry
import com.stanislawbrzezinski.contacts_list.statics.Routes


/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class ContactsAdapterViewModelImpl(
        contactsLiveData: ContactsLiveData,
        private val navigationLiveData:NavigationLiveData,
        private val selectedIdLiveData:SelectedContactIdLiveData)
    : ContactsAdapterViewModel {

    private val entriesLiveData = Transformations.map(contactsLiveData) {
        contactsLiveData.value?.map { it.toContactsEntry() }
    }

    override fun observeContacts(observer: Observer<List<ContactsEntryModel>?>) {
        entriesLiveData.observeForever(observer)
    }

    override fun removeContactsObserver(observer: Observer<List<ContactsEntryModel>?>) {
        entriesLiveData.removeObserver(observer)
    }

    override fun getContacts(): List<ContactsEntryModel>? {
        return entriesLiveData.value
    }

    override fun selectId(id:String){
        selectedIdLiveData.postValue(id)
        navigationLiveData.postValue(Routes.MAIN_TO_DETAILS)
    }
}