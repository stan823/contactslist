package com.stanislawbrzezinski.contacts_list.view_model

import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.SelectedContactIdLiveData
import com.stanislawbrzezinski.contacts_list.models.ContactModel
import com.stanislawbrzezinski.contacts_list.statics.Routes
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 03/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class ContactsAdapterViewModelImplTest {

    @RelaxedMockK
    private lateinit var contractsLiveData: ContactsLiveData

    @RelaxedMockK
    private lateinit var navigationLiveData: NavigationLiveData

    @RelaxedMockK
    private lateinit var selectedIdLiveData: SelectedContactIdLiveData

    private val adapterViewModel: ContactsAdapterViewModelImpl by lazy {
        ContactsAdapterViewModelImpl(contractsLiveData, navigationLiveData, selectedIdLiveData)
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testSelectId() {
        val ID = "15"
        adapterViewModel.selectId(ID)

        verify(exactly = 1) {
            selectedIdLiveData.postValue(ID)
        }

        verify(exactly = 1) {
            navigationLiveData.postValue(Routes.MAIN_TO_DETAILS)
        }
    }

}