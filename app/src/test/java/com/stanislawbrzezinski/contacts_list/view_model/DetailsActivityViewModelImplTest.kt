package com.stanislawbrzezinski.contacts_list.view_model

import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.SelectedContactIdLiveData
import com.stanislawbrzezinski.contacts_list.models.ContactModel
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 04/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class DetailsActivityViewModelImplTest {

    @RelaxedMockK
    private lateinit var navigationLiveData: NavigationLiveData

    @RelaxedMockK
    private lateinit var selectedContactIdLiveData: SelectedContactIdLiveData

    @RelaxedMockK
    private lateinit var contactsLiveData: ContactsLiveData


    private val viewModel: DetailsActivityViewModelImpl by lazy {
        DetailsActivityViewModelImpl(navigationLiveData, selectedContactIdLiveData, contactsLiveData)
    }


    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testDetailMapWhenNull() {

        val result = viewModel.detailsMap.invoke(null)
        assertEquals(0, result.size)
    }

    @Test
    fun testWhenMultipleItems() {
        val model = ContactModel(
                id = "id",
                name = "name",
                phones = listOf("phone1", "phone2"),
                emails = listOf("email1", "email2")
        )
        val result = viewModel.detailsMap.invoke(model)

        assertEquals(4, result.size)
        assertEquals("phone1",result[0])
        assertEquals("email2",result[3])
    }

    @Test
    fun testWhenSomeNullItems() {
        val model = ContactModel(
                id = "id",
                name = "name",
                phones = listOf(null, "phone2"),
                emails = listOf("email1", null)
        )
        val result = viewModel.detailsMap.invoke(model)

        assertEquals(2, result.size)
        assertEquals("phone2",result[0])
        assertEquals("email1",result[1])
    }
}