package com.stanislawbrzezinski.contacts_list.view_model

import com.stanislawbrzezinski.contacts_list.data_sources.ContactsProvider
import com.stanislawbrzezinski.contacts_list.live_datas.ContactsLiveData
import com.stanislawbrzezinski.contacts_list.live_datas.NavigationLiveData
import com.stanislawbrzezinski.contacts_list.models.ContactModel
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 04/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class MainActivityViewModelImplTest {

    @RelaxedMockK
    private lateinit var provider: ContactsProvider

    @RelaxedMockK
    private lateinit var navigationLiveData: NavigationLiveData

    @RelaxedMockK
    private lateinit var contactsLiveData: ContactsLiveData

    private val viewModel: MainActivityViewModelImpl by lazy {
        MainActivityViewModelImpl(provider, navigationLiveData, contactsLiveData)
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testFetchContacts() {
        val list = listOf(ContactModel("id", "name"))
        coEvery { provider.fetchContacts() } returns list

        viewModel.fetchContacts()

        coVerify(exactly = 1) {
            contactsLiveData.postValue(list)
        }

    }
}