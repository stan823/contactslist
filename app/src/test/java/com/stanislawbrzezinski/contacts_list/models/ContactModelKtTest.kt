package com.stanislawbrzezinski.contacts_list.models

import org.junit.Assert.*
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 04/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class ContactModelKtTest {
    @Test
    fun testToEntryModel() {
        val model = ContactModel(
                id = "id",
                name = "name",
                imageThumbUrl = "itu")

        val entry = model.toContactsEntry()
        assertEquals(entry.label, model.name)
        assertEquals(entry.imageUrl, model.imageThumbUrl)
        assertEquals(entry.id, model.id)
    }
}