package com.stanislawbrzezinski.contacts_list

import android.support.test.rule.ActivityTestRule
import com.stanislawbrzezinski.contacts_list.ui.MainActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.*
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.matcher.ViewMatchers.*


/**
 * Created by Stanisław Brzeziński on 04/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class DetailsTest {

    @get:Rule
    val rule = ActivityTestRule<MainActivity>(MainActivity::class.java, true, false)

    @Before
    fun setUp() {
        DebugFlags.isTest = true
        rule.launchActivity(null)
    }

    @Test
    fun testValuesWhenDetailsExists() {
        onView(RecyclerViewMatcher(R.id.entriesList)
                .atPositionOnView(0, R.id.card))
                .perform(click())

        onView(withId(R.id.tvName))
                .check(matches(withText("name1")))

        onView(RecyclerViewMatcher(R.id.detailsList)
                .atPositionOnView(1,R.id.tvEntry))
                .check(matches(withText("phone2")))

    }
}