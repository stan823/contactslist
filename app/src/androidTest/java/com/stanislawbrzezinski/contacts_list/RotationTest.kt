package com.stanislawbrzezinski.contacts_list

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.uiautomator.UiDevice
import com.stanislawbrzezinski.contacts_list.ui.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


/**
 * Created by Stanisław Brzeziński on 04/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class RotationTest {
    val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    @get:Rule
    val rule = ActivityTestRule<MainActivity>(MainActivity::class.java, true, false)

    @Before
    fun setUp() {
        DebugFlags.isTest = true
        rule.launchActivity(null)
    }

    @After
    fun tearDown() {
        device.setOrientationNatural()
    }

    @Test
    fun testList() {
        Espresso.onView(ViewMatchers.withId(R.id.entriesList))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(RecyclerViewMatcher(R.id.entriesList)
                .atPositionOnView(0, R.id.tvName)
        ).check(ViewAssertions.matches(ViewMatchers.withText("name1")))

        Espresso.onView(RecyclerViewMatcher(R.id.entriesList)
                .atPositionOnView(1, R.id.tvName)
        ).check(ViewAssertions.matches(ViewMatchers.withText("name2")))

        device.setOrientationLeft()

        Espresso.onView(RecyclerViewMatcher(R.id.entriesList)
                .atPositionOnView(0, R.id.tvName)
        ).check(ViewAssertions.matches(ViewMatchers.withText("name1")))

        Espresso.onView(RecyclerViewMatcher(R.id.entriesList)
                .atPositionOnView(1, R.id.tvName)
        ).check(ViewAssertions.matches(ViewMatchers.withText("name2")))

    }
}