package com.stanislawbrzezinski.contacts_list

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.*
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import com.stanislawbrzezinski.contacts_list.ui.MainActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test


/**
 * Created by Stanisław Brzeziński on 04/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class ContactsListTest {

    @get:Rule
    val rule = ActivityTestRule<MainActivity>(MainActivity::class.java, true, false)

    @Before
    fun setUp() {
        DebugFlags.isTest = true
        rule.launchActivity(null)
    }

    @Test
    fun testList() {
        onView(withId(R.id.entriesList))
                .check(matches(isDisplayed()))

        onView(RecyclerViewMatcher(R.id.entriesList)
                .atPositionOnView(0, R.id.tvName)
        ).check(matches(withText("name1")))

        onView(RecyclerViewMatcher(R.id.entriesList)
                .atPositionOnView(1, R.id.tvName)
        ).check(matches(withText("name2")))
    }
}